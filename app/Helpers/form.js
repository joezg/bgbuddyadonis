const reduceHelpers = require('./reduce');

exports.parseCheckboxes = (data, names) => {
  const newData = {
    ...data,
    ...names.reduce(
      reduceHelpers.toObjectField(name => {
        return {
          [name]: !!data.checkbox && data.checkbox.indexOf(name) > -1
        };
      }),
      {}
    )
  };

  delete newData.checkbox;
  return newData;
};
