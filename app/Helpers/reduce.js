exports.toObjectField = mapper => {
  return (object, current) => {
    return {
      ...object,
      ...mapper(current)
    };
  };
};
