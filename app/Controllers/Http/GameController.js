'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Game = use('App/Models/Game');

/**
 * Resourceful controller for interacting with games
 */
class GameController {
  /**
   * Show a list of all games.
   * GET games
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ view, auth }) {
    const games = await Game.query()
      .where('user_id', auth.user.id)
      .fetch();
    return view.render('games.index', { games: games.toJSON() });
  }

  /**
   * Render a form to be used for creating a new game.
   * GET games/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ view }) {
    return view.render('games.create');
  }

  /**
   * Create/save a new game.
   * POST games
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const data = request.getFormData(['name']);

    data.user_id = auth.user.id;
    await Game.create(data);

    return response.route('GameController.index');
  }

  /**
   * Display a single game.
   * GET games/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, response, view, auth }) {
    const game = await Game.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!game) {
      return response.notFound();
    }

    return view.render('games.show', { game });
  }

  /**
   * Render a form to update an existing game.
   * GET games/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, auth, response, view }) {
    const game = await Game.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!game) {
      return response.notFound();
    }

    return view.render('games.edit', { game });
  }

  /**
   * Update game details.
   * PUT or PATCH games/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth }) {
    const data = request.getFormData(['name']);
    const game = await Game.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!game) {
      return response.notFound();
    }

    game.merge(data);
    await game.save();

    return response.route('GameController.index');
  }

  /**
   * Delete a game with id.
   * DELETE games/:id
   * GET games/:id/delete
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, response, auth }) {
    const game = await Game.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!game) {
      return response.notFound();
    }
    await game.delete();
    return response.route('GameController.index');
  }
}

module.exports = GameController;
