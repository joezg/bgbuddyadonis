'use strict';

class UserController {
  async login({ view }) {
    return view.render('user.login');
  }

  async logout({ auth, response }) {
    await auth.logout();
    return response.route('home');
  }

  async authenticate({ auth, request, response }) {
    const { email, password } = request.all();
    await auth.attempt(email, password);

    return response.route('home');
  }
}

module.exports = UserController;
