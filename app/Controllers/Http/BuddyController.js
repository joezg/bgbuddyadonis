'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Buddy = use('App/Models/Buddy');

/**
 * Resourceful controller for interacting with buddies
 */
class BuddyController {
  /**
   * Show a list of all buddies.
   * GET buddies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ view, auth }) {
    const buddies = await Buddy.query()
      .where('user_id', auth.user.id)
      .fetch();
    return view.render('buddies.index', { buddies: buddies.toJSON() });
  }

  /**
   * Render a form to be used for creating a new buddy.
   * GET buddies/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ view }) {
    return view.render('buddies.create');
  }

  /**
   * Create/save a new buddy.
   * POST buddies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const data = request.getFormData(['name', 'favourite']);

    data.user_id = auth.user.id;
    await Buddy.create(data);

    return response.route('BuddyController.index');
  }

  /**
   * Display a single buddy.
   * GET buddies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, response, view, auth }) {
    const buddy = await Buddy.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!buddy) {
      return response.notFound();
    }

    return view.render('buddies.show', { buddy });
  }

  /**
   * Render a form to update an existing buddy.
   * GET buddies/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, response, view, auth }) {
    const buddy = await Buddy.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!buddy) {
      return response.notFound();
    }

    return view.render('buddies.edit', { buddy });
  }

  /**
   * Update buddy details.
   * PUT or PATCH buddies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth }) {
    const data = request.getFormData(['name', 'favourite']);
    const buddy = await Buddy.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!buddy) {
      return response.notFound();
    }

    buddy.merge(data);
    await buddy.save();

    return response.route('BuddyController.index');
  }

  /**
   * Delete a buddy with id.
   * DELETE buddies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, response, auth }) {
    const buddy = await Buddy.query()
      .where('id', params.id)
      .andWhere('user_id', auth.user.id)
      .first();

    if (!buddy) {
      return response.notFound();
    }
    await buddy.delete();
    return response.route('BuddyController.index');
  }
}

module.exports = BuddyController;
