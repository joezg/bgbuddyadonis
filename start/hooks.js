const { hooks } = require('@adonisjs/ignitor');
const formHelpers = require('../app/Helpers/form');

hooks.before.httpServer(() => {
  const Request = use('Adonis/Src/Request');

  Request.macro('getFormData', function(keys) {
    let values = this.only([...keys, 'checkbox']);

    const checkboxKeys = keys.filter(key => values[key] === undefined);
    values = formHelpers.parseCheckboxes(values, checkboxKeys);
    return values;
  });
});
