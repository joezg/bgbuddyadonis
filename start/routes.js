'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/login', 'UserController.login').middleware('guest');
Route.get('/logout', 'UserController.logout').middleware('auth');
Route.post('/authenticate', 'UserController.authenticate').middleware('guest');

Route.on('/')
  .render('welcome')
  .as('home');

Route.group(() => {
  Route.get('games/:id/delete', 'GameController.destroy');
  Route.resource('games', 'GameController');

  Route.get('buddies/:id/delete', 'BuddyController.destroy');
  Route.resource('buddies', 'BuddyController');

  Route.get('location/:id/delete', 'LocationController.destroy');
  Route.resource('locations', 'LocationController');
}).middleware('auth');
