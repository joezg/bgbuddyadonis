'use strict';

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');
const User = use('App/Models/User');
const Env = use('Env');

class UserSeeder {
  async run() {
    if (Env.get('ADD_DEFAULT_USER') === 'true'){
      await User.create({
        username: Env.get('DEFAULT_USER_USERNAME'),
        email: Env.get('DEFAULT_USER_EMAIL'),
        password: Env.get('DEFAULT_USER_PASSWORD')
      });
    }

  }
}

module.exports = UserSeeder;
