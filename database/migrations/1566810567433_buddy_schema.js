'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class BuddySchema extends Schema {
  up() {
    this.create('buddies', table => {
      table.increments();
      table
        .integer('user_id')
        .notNullable()
        .references('id')
        .inTable('users');
      table.string('name', 1023).notNullable();
      table
        .boolean('favourite')
        .nullable()
        .default(false);
      table.timestamps();
    });
  }

  down() {
    this.drop('buddies');
  }
}

module.exports = BuddySchema;
